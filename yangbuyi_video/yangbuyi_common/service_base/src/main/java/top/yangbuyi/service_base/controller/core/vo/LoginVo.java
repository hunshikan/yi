// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_base.controller.core.vo;

import lombok.Data;

/**
 * @program: yangbuyi_video
 * @ClassName: LoginVo
 * @create: 2021-10-13 01:07
 * @author: Yang Shuai
 * @since： JDK1.8
 * @LoginVo: 门户页面登陆$
 **/

@Data
public class LoginVo {
    /**手机号*/
    private String phone;
    /**密码*/
    private String password;
}

