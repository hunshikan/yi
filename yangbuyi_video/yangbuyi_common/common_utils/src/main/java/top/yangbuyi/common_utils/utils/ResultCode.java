// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.common_utils.utils;


/**
 * 结果代码
 *
 * @author Yang Shuai
 * @program: yangbuyi_viedo
 * @ClassName: ResultCode
 * @create: 2021-09-05 23:11
 * @author: Yang Shuai
 * @since： JDK1.8
 * @ResultCode: $
 * @date 2021/09/05
 */
public interface ResultCode {
    /**
     * 成功
     */
    Integer SUCCESS = 20000;
    /**
     * 错误
     */
    Integer ERROR = 20001;
}
