// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.common_utils.constant;

/**
 * @program: yangbuyi_video
 * @ClassName: OrderConstant
 * @create: 2021/10/21:17:41
 * @author: Yang Shuai
 * @desc: 订单常量|
 **/
public class OrderConstant {

    /**
     * 支付成功 1
     */
    public static final Integer STATUS_1 = 1;

}
