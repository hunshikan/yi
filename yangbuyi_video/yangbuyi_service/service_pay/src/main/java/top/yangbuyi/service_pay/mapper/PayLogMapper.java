// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_pay.mapper;

import top.yangbuyi.service_pay.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author yangbuyiya
 * @since 2021-10-20
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
