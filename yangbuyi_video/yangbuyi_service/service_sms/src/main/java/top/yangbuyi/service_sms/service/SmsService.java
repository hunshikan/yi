// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_sms.service;

import org.springframework.cloud.openfeign.FeignClient;

import java.util.Map;

/**
 * @program: yangbuyi_video
 * @ClassName: SmsService
 * @create: 2021-10-12 23:44
 * @author: Yang Shuai
 * @since： JDK1.8
 * @SmsService: $
 **/

public interface SmsService {
    /**
     * 短信发送验证码
     * @param phone
     * @param param
     * @return boolean
     */
    boolean send (String phone, Map<String, Object> param);
}
