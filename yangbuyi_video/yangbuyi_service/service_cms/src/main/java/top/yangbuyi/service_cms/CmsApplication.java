// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_cms;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import top.yangbuyi.common_utils.annotation.EnableCustomConfig;
import top.yangbuyi.common_utils.annotation.EnableYsFeignClients;

/**
 * @program: yangbuyi_video
 * @ClassName: CmsApplication
 * @create: 2021-10-11 23:16
 * @author: Yang Shuai
 * @since： JDK1.8
 * @CmsApplication: $
 **/


@EnableCustomConfig
@EnableYsFeignClients
@SpringCloudApplication
public class CmsApplication {
    public static void main (String[] args) {
        SpringApplication.run(CmsApplication.class, args);
    }
}

