// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_cms.mapper;

import top.yangbuyi.service_cms.entity.Banner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author yangbuyi
 * @since 2021-10-11
 */
public interface BannerMapper extends BaseMapper<Banner> {

}
