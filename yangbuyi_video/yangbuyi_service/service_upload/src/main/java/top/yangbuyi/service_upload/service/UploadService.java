// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_upload.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @program: yangbuyi_video
 * @ClassName: UploadService
 * @create: 2021-09-08 02:01
 * @author: Yang Shuai
 * @since： JDK1.8
 * @UploadService: $
 **/
public interface UploadService {
    /**
     * 阿里云上传文件
     *
     * @param file 文件
     * @return {@link String}
     */
    String uploadFile(MultipartFile file);

    /**
     * 七牛云OSS上传
     * @param file
     * @return 地址
     */
    String qiniuOssUploadFile(MultipartFile file);
}
