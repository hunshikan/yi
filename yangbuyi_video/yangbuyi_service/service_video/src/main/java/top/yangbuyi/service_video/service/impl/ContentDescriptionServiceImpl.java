// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_video.service.impl;

import top.yangbuyi.api_video.entity.ContentDescription;
import top.yangbuyi.service_video.mapper.ContentDescriptionMapper;
import top.yangbuyi.service_video.service.ContentDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 作品简介 服务实现类
 * </p>
 *
 * @author yangbuyi
 * @since 2021-09-19
 */
@Service
public class ContentDescriptionServiceImpl extends ServiceImpl<ContentDescriptionMapper, ContentDescription> implements ContentDescriptionService {

}
