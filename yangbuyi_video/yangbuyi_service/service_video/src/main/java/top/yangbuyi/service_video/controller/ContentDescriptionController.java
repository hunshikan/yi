// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_video.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 作品简介 前端控制器
 * </p>
 *
 * @author yangbuyi
 * @since 2021-09-19
 */
@RestController
@RequestMapping("/service_video/content-description")
public class ContentDescriptionController {

}

