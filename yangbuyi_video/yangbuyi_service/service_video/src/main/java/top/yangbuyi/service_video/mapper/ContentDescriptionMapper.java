// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_video.mapper;

import top.yangbuyi.api_video.entity.ContentDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 作品简介 Mapper 接口
 * </p>
 *
 * @author yangbuyi
 * @since 2021-09-19
 */
public interface ContentDescriptionMapper extends BaseMapper<ContentDescription> {

}
