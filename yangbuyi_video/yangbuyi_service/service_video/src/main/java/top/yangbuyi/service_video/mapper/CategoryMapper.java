// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_video.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.yangbuyi.api_video.entity.Category;

/**
*  @program:  yangbuyi_video
*  @ClassName:  CategoryMapper
*  @create:  2021-09-09 01:09
*  @author:  Yang Shuai
*  @since： JDK1.8   
*  @CategoryMapper: ${desc}$
**/
public interface CategoryMapper extends BaseMapper<Category> {
}