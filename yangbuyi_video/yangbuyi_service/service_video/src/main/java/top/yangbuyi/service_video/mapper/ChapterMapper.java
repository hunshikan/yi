// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_video.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import feign.Param;
import top.yangbuyi.api_video.entity.Chapter;

/**
 * <p>
 * 作品章节 Mapper 接口
 * </p>
 *
 * @author yangbuyi
 * @since 2021-09-19
 */
public interface ChapterMapper extends BaseMapper<Chapter> {

    Integer getContentOrVideoSortMax (@Param("type") Integer type);
}
