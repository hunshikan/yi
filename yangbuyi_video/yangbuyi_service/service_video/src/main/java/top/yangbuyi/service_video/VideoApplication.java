// Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
// Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
// Please keep the information of the original author of the code. Thank you

package top.yangbuyi.service_video;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import top.yangbuyi.common_utils.annotation.EnableCustomConfig;
import top.yangbuyi.common_utils.annotation.EnableYsFeignClients;

/**
 * @program: yangbuyi_viedo
 * @ClassName: VideoApplication
 * @create: 2021-09-05 22:27
 * @author: Yang Shuai
 * @since： JDK1.8
 * @VideoApplication: 视频服务入口$
 **/

@EnableCustomConfig
@EnableYsFeignClients
@SpringCloudApplication
public class VideoApplication {

    public static void main (String[] args) {
        SpringApplication.run(VideoApplication.class, args);
    }

}
