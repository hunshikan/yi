/*
 * Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
 * Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
 * Please keep the information of the original author of the code. Thank you
 */

import request from '@/utils/request'
const api_name = '/service_video/author'

export default {
  authorWebList(page, limit) {
    return request({
      url: `${api_name}/pageList/${page}/${limit}`,
      method: 'post'
    })
  },
  getAuthorById(authorId) {
    return request({
      url: `${api_name}/getAuthorWithId/${authorId}`,
      method: 'get'
    })
  }

}
