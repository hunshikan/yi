/*
 * Yang Shuai  Copyright (c) 2022 https://yangbuyi.top.
 * Copyright (c) 2022. Yangbuyi, personal projects are not allowed to be commercialized without permission.
 * Please keep the information of the original author of the code. Thank you
 */

// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = {
  'plugins': {
    // to edit target browsers: use "browserslist" field in package.json
    'autoprefixer': {}
  }
}
